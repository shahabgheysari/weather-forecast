﻿using System;
using WeatherInquiry;
namespace AdministratorInquiry
{
    class Program
    {
        static void Main(string[] args)
        {
            IInquiryWeather inquiryWeather = new InquiryWeather(null);


            Console.WriteLine("Enter a city:");
            string city  = Console.ReadLine();
            Console.WriteLine(inquiryWeather.Forecast(city));
        }
    }
}