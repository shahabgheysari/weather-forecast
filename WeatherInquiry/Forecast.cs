namespace WeatherInquiry
{
    public class Forecast
    {
        private readonly Temperature _temperature;
        private readonly Weather _weather;

        public Forecast(Temperature temperature, Weather weather)
        {
            _temperature = temperature;
            _weather = weather;
        }

        public string Status()
        {
            return $"{_weather.Description()}\nTemperature={_temperature.Celsius()}C";
        }
    }
}