using System.Collections.Specialized;
using NSubstitute;
using Xunit;

namespace WeatherInquiry
{
    public class ForecastService
    {
        [Fact]
        public void TestForecastService()
        {
            IForecastService forecastService = Substitute.For<IForecastService>();
            forecastService.GetForecastValues("Tehran").Returns(new NameValueCollection()
            {
                {"temperature","35"},
                {"weather","Rainy"}
            });
            
            IInquiryWeather inquiryWeather = new InquiryWeather(forecastService);
            string result = inquiryWeather.Forecast("Tehran");
            
            Assert.Equal("Tehran\nRainy\nTemperature=35C",result);

        }
    }
}