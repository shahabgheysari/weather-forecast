using Xunit;

namespace WeatherInquiry
{
    public class ForecastUnitTest
    {
        [Fact]
        public void TestTemperatureHasADegrees()
        {
            Temperature temperature = new Temperature(45);
            
            Assert.Equal(45,temperature.Celsius());
        }

        [Fact]
        public void TestWeatherHasDescription()
        {
            Weather weather = new Weather("cloudy");
            
            Assert.Equal("cloudy",weather.Description());
        }

        [Fact]
        public void TestForecastResult()
        {
            Forecast forecast = new Forecast(new Temperature(30),new Weather("Rainy"));
            
            Assert.Equal("Rainy\nTemperature=30C",forecast.Status());
            
        }
    }
}