using System.Collections.Specialized;

namespace WeatherInquiry
{
    public interface IForecastService
    {
        NameValueCollection GetForecastValues(string city);
    }
}