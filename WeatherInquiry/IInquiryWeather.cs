namespace WeatherInquiry
{
    public interface IInquiryWeather
    {
        string Forecast(string city);
    }
}