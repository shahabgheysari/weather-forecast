using Xunit;

namespace WeatherInquiry
{
    public class InquiryUnitTest
    {
        [Fact]
        public void TestInquiryWeatherHasACityName()
        {
            IInquiryWeather inquiryWeather = new InquiryWeather(null);
            string forecast = inquiryWeather.Forecast("Tehran");
            
            Assert.NotEmpty(forecast);
        }

        [Fact]
        public void TestInquiryCityInResult()
        {
            IInquiryWeather inquiryWeather = new InquiryWeather(null);
            string forecast = inquiryWeather.Forecast("Tehran");
            
            Assert.Contains("Tehran",forecast);
        }
    }
}