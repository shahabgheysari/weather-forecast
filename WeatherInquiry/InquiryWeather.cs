using System.Collections.Specialized;

namespace WeatherInquiry
{
    public class InquiryWeather:IInquiryWeather
    {
        private readonly IForecastService _forecastService;

        public InquiryWeather(IForecastService forecastService)
        {
            _forecastService = forecastService;
        }
        public string Forecast(string city)
        {
            if (_forecastService != null)
            {
                NameValueCollection data = _forecastService.GetForecastValues(city);

                return Format(city, float.Parse(data.Get("temperature")),data.Get("weather"));
            }
            return $"{city}\nRainy\nTemperature=30C";
        }

        private string Format(string city, float temperature,string weather)
        {
            return
                $"{city}\n{weather}\nTemperature={new Temperature(temperature).Celsius()}C";
        }
    }
}