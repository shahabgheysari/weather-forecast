namespace WeatherInquiry
{
    public class Temperature
    {
        private readonly float _degrees;

        public Temperature(float degrees)
        {
            _degrees = degrees;
        }

        public float Celsius()
        {
            return _degrees;
        }
    }
}