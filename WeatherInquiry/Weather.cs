namespace WeatherInquiry
{
    public class Weather
    {
        private readonly string _description;

        public Weather(string description)
        {
            _description = description;
        }

        public string Description()
        {
            return _description;
        }
    }
}